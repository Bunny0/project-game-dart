import 'dart:io';
import 'package:dart_console/dart_console.dart';

class EffectGame {
  final console = Console();
  void LogoGame() {
    print(
        // ignore: prefer_single_quotes
        r"""\
                                                                           =============================================================
                                                                           ||    _____              __  __   ______     ___      __   ||
                                                                           ||   / ____|     /\     |  \/  | |  ____|   |__ \    /_ |  ||
                                                                           ||  | |  __     /  \    | \  / | | |__         ) |    | |  ||
                                                                           ||  | | |_ |   / /\ \   | |\/| | |  __|       / /     | |  ||
                                                                           ||  | |__| |  / ____ \  | |  | | | |____     / /_     | |  ||
                                                                           ||   \_____| /_/    \_\ |_|  |_| |______|   |____|    |_|  ||
                                                                           ||                                                         ||
                                                                           =============================================================

                """);
  }

  void EndGame() {
    print(
        // ignore: prefer_single_quotes
        r"""\
                                                                 ____             ____                _____                 _ _                _    
                                                                 |  _ \           |  _ \              / ____|               | | |              | |   
                                                                 | |_) |_   _  ___| |_) |_   _  ___  | |  __  ___   ___   __| | |    _   _  ___| | __
                                                                 |  _ <| | | |/ _ |  _ <| | | |/ _ \ | | |_ |/ _ \ / _ \ / _` | |   | | | |/ __| |/ /
                                                                 | |_) | |_| |  __| |_) | |_| |  __/ | |__| | (_) | (_) | (_| | |___| |_| | (__|   < 
                                                                 |____/ \__, |\___|____/ \__, |\___|  \_____|\___/ \___/ \__,_|______\__,_|\___|_|\_\
                                                                         __/ |            __/ |                                                      
                                                                        |___/            |___/                                                       
                """);
  }

  void TabLoadGame() {
    // ignore: unused_local_variable
    var time = 0;
    for (var i = 0; i < 201; i++) {
      stdout.write('[');

      stdout.write('=' * time);
      stdout.write(' ' * ((stdout.terminalColumns - 7) - time));
      stdout.write(']${(i / 2).round()} %');
      sleep(Duration(milliseconds: 80));
      print('\x1B[2J\x1B[0;0H');
      time++;
    }
  }

  void GameRules() {
    // ignore: omit_local_variable_types
    List dialog = [
      '>RULSE TO PLAY THIS GAME<',
      'Winning in each round is governed by the following rules',
      'you just draw card, trying to add up to twenty-one',
      'go over twenty-one and you lose!',
      'The player who reaches all 3 points first is the winner.'
    ];
    print('=' * stdout.terminalColumns);
    for (var i = 0; i < dialog.length; i++) {
      print(' ' * ((stdout.terminalColumns - dialog[i].length) / 2).round() +
          dialog[i]);
    }
    print('=' * stdout.terminalColumns);
    print('\n');
  }

  void ClearGame() {
    console.clearScreen();
    console.resetCursorPosition();
    EffectGame().EndGame();
    console.writeLine();
  }
}
