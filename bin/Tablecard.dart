import 'dart:math';
import 'dart:io';
import 'AbsCard.dart';
import 'PointRound.dart';
import 'EffectGame.dart';
import 'MixinSpecialcard.dart';
import 'Player.dart';

class Tablecard extends AbsCard with MixinSecialcard {
  //name && card && Point && PointRound : Player1
  Player p1 = Player();
  List<int> CardPlayer1 = [];
  int Point_TotalPlayer1 = 0;
  PointRound PointRoundPlayer1 = PointRound(0);

  //name && card && Point && PointRound : Player2
  Player p2 = Player();
  List<int> CardPlayer2 = [];
  int Point_TotalPlayer2 = 0;
  PointRound PointRoundPlayer2 = PointRound(0);

  //DrawGame
  PointRound draw = PointRound(0);

  //MaxPoint
  int maxpoint = 21;

  //ListAllCard
  List<int> card = [
    1,
    2,
    3,
    4,
    5,
    6,
    7,
    8,
    9,
    10,
    11,
    12,
    13,
    14,
    15,
    16,
    17,
    18,
    19,
    20,
    21
  ];

  //randomcard
  var rngcard = Random();

  //start
  bool startloop = true;
  @override
  void StartGame() {
    EffectGame().TabLoadGame();
    EffectGame().LogoGame();
    EffectGame().GameRules();
    while (startloop) {
      stdout.write(' ' *
              ((stdout.terminalColumns -
                          'Want to start the game? Y/N : '.length) /
                      2)
                  .round() +
          'Want to start the game? Y/N : ');
      var start = (stdin.readLineSync()!);
      // var start = 'y';
      if (start == 'Y' || start == 'y') {
        InputName();

        while (true) {
          if (PointRoundPlayer1.getpointRound == 3) {
            EffectGame().ClearGame();
            Matchpoint();
            print('\n');
            print(' ' *
                    ((stdout.terminalColumns - ' $p1 Win!!! '.length) / 2)
                        .round() +
                ' $p1 Win!!! ');

            startloop = false;
            break;
          } else if (PointRoundPlayer2.getpointRound == 3) {
            EffectGame().ClearGame();
            Matchpoint();
            print('\n');
            print(' ' *
                    ((stdout.terminalColumns - '$p2 Win!!!'.length) / 2)
                        .round() +
                '$p2 Win!!!');
            startloop = false;
            break;
          }
          Matchpoint();
          print('');
          FirstCard();
          Deal();
          Specialcard();
          CheckTotalPoint();
          //กฏการชนะ
          Lastturn();
          //newGame
          NewRound();
          continue;
        }
      } else if (start == 'N' || start == 'n') {
        print(
            ' ' * ((stdout.terminalColumns - 'Exit Game'.length) / 2).round() +
                'Exit Game');
        EffectGame().ClearGame();
        break;
      } else {
        print(' ' *
                ((stdout.terminalColumns - 'Please enter Y or N'.length) / 2)
                    .round() +
            'Please enter Y or N');
        continue;
      }
    }
  }

  @override
  void InputName() {
    print('\n');
    stdout.write(
        ' ' * ((stdout.terminalColumns - dialog[1].length) / 2).round() +
            dialog[1]);
    var Inputplayer1 = stdin.readLineSync();
    p1.player = Inputplayer1!;
    print('-' * stdout.terminalColumns);
    stdout.write(
        ' ' * ((stdout.terminalColumns - dialog[1].length) / 2).round() +
            dialog[1]);
    var Inputplayer2 = stdin.readLineSync();
    p2.player = Inputplayer2!;

    print('\n \n \n \n \n \n \n \n \n \n \n \n');
  }

  @override
  void Matchpoint() {
    // ignore: omit_local_variable_types
    List match = [
      '============> Matchpoint <=============',
      '$p1 win =$PointRoundPlayer1',
      '$p2 win =$PointRoundPlayer2',
      'Darw =$draw'
    ];
    for (var i = 0; i < match.length; i++) {
      print(' ' * ((stdout.terminalColumns - match[i].length) / 2).round() +
          match[i]);
    }
  }

  @override
  void FirstCard() {
    print('\n \n \n');
    print(' ' * ((stdout.terminalColumns - dialog[0].length) / 2).round() +
        dialog[0]);
    var element1 = 0;
    var element2 = 0;
    for (var i = 0; i < 4; i++) {
      if (i == 0 || i == 2) {
        element1 = card[rngcard.nextInt(card.length)];
        CardPlayer1.add(element1);
        Point_TotalPlayer1 = Point_TotalPlayer1 + element1;
        card.remove(element1);
      } else if (i == 1 || i == 3) {
        element2 = card[rngcard.nextInt(card.length)];
        CardPlayer2.add(element2);
        Point_TotalPlayer2 = Point_TotalPlayer2 + element2;
        card.remove(element2);
      }
    }
    CheckTotalPoint();
  }

  @override
  void CheckTotalPoint() {
    for (var i = 0; i < 9; i++) {
      print('\n');
    }
    print('');
    stdout.write(' ' *
            ((stdout.terminalColumns - '===> ScoreBoard <==='.length) / 2)
                .round() +
        '===> ScoreBoard <===');
    print('\n');
    stdout.write(' ' *
            ((stdout.terminalColumns - 'MaxPoint = $maxpoint'.length) / 2)
                .round() +
        'MaxPoint = $maxpoint');
    print('\n');
    // ignore: omit_local_variable_types
    List dialog = [
      'Number of cards in the pile : $card',
      '================Player1=================',
      '================Player2================='
    ];
    print(' ' * ((stdout.terminalColumns - dialog[0].length) / 2).round() +
        dialog[0]);
    print('=' * stdout.terminalColumns);
    print(' ' * ((stdout.terminalColumns - dialog[1].length) / 2).round() +
        dialog[1]);
    print(' ' * ((stdout.terminalColumns - p1.toString().length) / 2).round() +
        p1.toString());

    print(' ' *
            ((stdout.terminalColumns - 'CardPlayer1 = $CardPlayer1'.length) / 2)
                .round() +
        'CardPlayer1 = $CardPlayer1');
    print(' ' *
            ((stdout.terminalColumns -
                        'Point_TotalPlayer1 = $Point_TotalPlayer1'.length) /
                    2)
                .round() +
        'Point_TotalPlayer1 = $Point_TotalPlayer1');
    print('=' * stdout.terminalColumns);
    //
    print(' ' * ((stdout.terminalColumns - dialog[2].length) / 2).round() +
        dialog[2]);
    print(' ' * ((stdout.terminalColumns - p2.toString().length) / 2).round() +
        p2.toString());

    print(' ' *
            ((stdout.terminalColumns - 'CardPlayer2 = $CardPlayer2'.length) / 2)
                .round() +
        'CardPlayer2 = $CardPlayer2');
    print(' ' *
            ((stdout.terminalColumns -
                        'Point_TotalPlayer2 = $Point_TotalPlayer2'.length) /
                    2)
                .round() +
        'Point_TotalPlayer2 = $Point_TotalPlayer2');
    print('=' * stdout.terminalColumns);
  }

  @override
  void Deal() {
    DealorPassPlayer1();
    DealorPassPlayer2();
    CheckTotalPoint();
  }

  @override
  void DealorPassPlayer1() {
    stdout.write(' ' *
            ((stdout.terminalColumns - 'Player 1 Deal or Pass Y/N : '.length) /
                    2)
                .round() +
        'Player 1 Deal or Pass Y/N : ');
    var play1 = (stdin.readLineSync()!);
    if (play1 == 'Y' || play1 == 'y') {
      print('');
      var element1 = 0;
      element1 = card[rngcard.nextInt(card.length)];
      CardPlayer1.add(element1);
      Point_TotalPlayer1 = Point_TotalPlayer1 + element1;
      card.remove(element1);
      print(' ' * ((stdout.terminalColumns - dialog[3].length) / 2).round() +
          dialog[3]);
    } else {
      print('');
      print(' ' * ((stdout.terminalColumns - dialog[2].length) / 2).round() +
          dialog[2]);
    }
    print('');
  }

  @override
  void DealorPassPlayer2() {
    stdout.write(' ' *
            ((stdout.terminalColumns - 'Player 2 Deal or Pass Y/N : '.length) /
                    2)
                .round() +
        'Player 2 Deal or Pass Y/N : ');
    var play2 = (stdin.readLineSync()!);
    if (play2 == 'Y' || play2 == 'y') {
      print('');
      var element2 = 0;
      element2 = card[rngcard.nextInt(card.length)];
      CardPlayer2.add(element2);
      Point_TotalPlayer2 = Point_TotalPlayer2 + element2;
      card.remove(element2);
      print(' ' * ((stdout.terminalColumns - dialog[3].length) / 2).round() +
          dialog[3]);
    } else {
      print('');
      print(' ' * ((stdout.terminalColumns - dialog[2].length) / 2).round() +
          dialog[2]);
    }
    print('');
  }

  @override
  void Specialcard() {
    var RngSp = Random();
    var randomspecialplayer1 = 1 + rngspecialcard.nextInt(11 - 1);
    var randomspecialplayer2 = 1 + rngspecialcard.nextInt(11 - 1);

    print('');
    stdout.write(' ' *
            ((stdout.terminalColumns - '♠ Turn Special Card ♠'.length) / 2)
                .round() +
        '♠ Turn Special Card ♠');
    print('\n');
    for (var i = 0; i < Description.length; i++) {
      print(
          ' ' * ((stdout.terminalColumns - Description[i].length) / 2).round() +
              Description[i]);
    }
    print('\n');
    print(' ' *
            ((stdout.terminalColumns -
                        '🔄Random Player Pick Special Card'.length) /
                    2)
                .round() +
        '🔄Random Player Pick Special Card');
    print('');
    // ignore: unrelated_type_equality_checks
    if (RngSp.nextInt(2) == 0) {
      SpecialcardPlayer1(randomspecialplayer1);
      SpecialcardPlayer2(randomspecialplayer2);
    } else {
      SpecialcardPlayer2(randomspecialplayer2);
      SpecialcardPlayer1(randomspecialplayer1);
    }
    print('=' * stdout.terminalColumns);
  }

  @override
  int SpecialcardPlayer1(randomspecialplayer1) {
    print('');
    stdout.write(' ' *
            ((stdout.terminalColumns -
                        '$p1 => 🃏pick up Special Card : $randomspecialplayer1'
                            .length) /
                    2)
                .round() +
        '$p1 => 🃏pick up Special Card : $randomspecialplayer1');
    print('\n');
    stdout.write(
        ' ' * ((stdout.terminalColumns - dialog[4].length) / 2).round() +
            dialog[4]);
    var x1 = stdin.readLineSync();
    if (x1 == 'Y' || x1 == 'y') {
      var i1 = RandomSpecialcardPlayer(
          Point_TotalPlayer1, Point_TotalPlayer2, randomspecialplayer1);

      Point_TotalPlayer1 = i1;
    } else {
      print('');
      print(' ' * ((stdout.terminalColumns - dialog[5].length) / 2).round() +
          dialog[5]);
    }
    print('\n');
    return Point_TotalPlayer1;
  }

  @override
  int SpecialcardPlayer2(randomspecialplayer2) {
    print('');
    stdout.write(' ' *
            ((stdout.terminalColumns -
                        '$p2 => 🃏pick up Special Card : $randomspecialplayer2'
                            .length) /
                    2)
                .round() +
        '$p2 => 🃏pick up Special Card : $randomspecialplayer2');
    print('\n');
    stdout.write(
        ' ' * ((stdout.terminalColumns - dialog[4].length) / 2).round() +
            dialog[4]);
    var x2 = stdin.readLineSync();
    if (x2 == 'Y' || x2 == 'y') {
      var i2 = RandomSpecialcardPlayer(
          Point_TotalPlayer2, Point_TotalPlayer1, randomspecialplayer2);

      Point_TotalPlayer2 = i2;
    } else {
      print('');
      print(' ' * ((stdout.terminalColumns - dialog[5].length) / 2).round() +
          dialog[5]);
    }
    print('');
    return Point_TotalPlayer2;
  }

  @override
  int RandomSpecialcardPlayer(int input1, int input2, int rng) {
    var i = 0;
    if (rng == 1) {
      i = specialcard1(input1);
    } else if (rng == 2) {
      i = specialcard2(input1);
    } else if (rng == 3) {
      i = specialcard3(input1);
    } else if (rng == 4) {
      maxpoint = 27;
      i = input1;
    } else if (rng == 5) {
      maxpoint = 35;
      i = input1;
    } else if (rng == 6) {
      i = specialcard6(input1);
    } else if (rng == 7) {
      i = specialcard7(input1);
    } else if (rng == 8) {
      i = specialcard8(input1);
    } else if (rng == 9) {
      i = specialcard9(input1);
    } else if (rng == 10) {
      i = specialcard10(input1);
    }
    return i;
  }

  @override
  void Lastturn() {
    if (Point_TotalPlayer1 == maxpoint && Point_TotalPlayer2 == maxpoint) {
      draw.RoundCount(); //T
    } else if (Point_TotalPlayer1 < maxpoint && Point_TotalPlayer2 > maxpoint) {
      PointRoundPlayer1.RoundCount(); //T
    } else if (Point_TotalPlayer2 < maxpoint && Point_TotalPlayer1 > maxpoint) {
      PointRoundPlayer2.RoundCount(); //T
    } else if (Point_TotalPlayer1 == Point_TotalPlayer2 &&
        Point_TotalPlayer1 < maxpoint &&
        Point_TotalPlayer2 < maxpoint) {
      draw.RoundCount();
    } else if (Point_TotalPlayer1 < maxpoint && Point_TotalPlayer2 < maxpoint) {
      var i1 = 0;
      var i2 = 0;
      i1 = maxpoint - Point_TotalPlayer1;
      i2 = maxpoint - Point_TotalPlayer2;
      if (i1 < i2) {
        PointRoundPlayer1.RoundCount();
      } else {
        PointRoundPlayer2.RoundCount();
      }
    } else if (Point_TotalPlayer1 == maxpoint) {
      PointRoundPlayer1.RoundCount(); //T
    } else if (Point_TotalPlayer2 == maxpoint) {
      PointRoundPlayer2.RoundCount(); //T
    } else if (Point_TotalPlayer1 < maxpoint &&
        Point_TotalPlayer2 < Point_TotalPlayer1) {
      PointRoundPlayer1.RoundCount();
    } else if (Point_TotalPlayer2 < maxpoint &&
        Point_TotalPlayer1 < Point_TotalPlayer2) {
      PointRoundPlayer2.RoundCount();
    } else if (Point_TotalPlayer1 > maxpoint && Point_TotalPlayer2 > maxpoint) {
      draw.RoundCount();
    }
    print('\n \n \n \n \n \n \n \n \n \n \n');
  }

  @override
  void NewRound() {
    // ignore: omit_local_variable_types
    List<int> Newcard = [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13,
      14,
      15,
      16,
      17,
      18,
      19,
      20,
      21
    ];
    card = Newcard;
    Point_TotalPlayer1 = 0;
    Point_TotalPlayer2 = 0;
    CardPlayer1 = [];
    CardPlayer2 = [];
    maxpoint = 21;
  }

  List dialog = [
    '2 cards will be dealt for the first time',
    'Enter player name : ', //(Enter a name up to 22 characters )
    'Players do not pick cards.',
    'Players pick cards.',
    'Do players want to use special cards? Y/N :',
    'Players do not use special cards.'
  ];
  List Description = [
    'specialcard 1 point +10  ',
    'specialcard 2 point -5 ',
    'specialcard 3 point -10',
    'specialcard 4 maxpoint = 27',
    'specialcard 5 maxpoint = 35',
    'specialcard 6 point -7',
    'specialcard 7 point -9',
    'specialcard 8 point +7',
    'specialcard 9 point +5',
    'specialcard 10 point +2'
  ];
}
